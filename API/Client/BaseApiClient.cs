﻿using Allure.Commons;
using OpenQA.Selenium.DevTools.V117.Debugger;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Client
{
    internal class BaseApiClient
    {
        private readonly RestClient client;
        public BaseApiClient(string baseUrl)
        {
            client = new RestClient(baseUrl);
        }
        private async Task<T> ExecuteMyRequest<T>(RestRequest request) where T : new()
        {
            var response = await client.ExecuteAsync<T>(request);
            var data = response.Data;
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented);
            byte[] log_ = Encoding.ASCII.GetBytes(json);
            AllureLifecycle.Instance.AddAttachment("APIResponse", "application/json", log_, "json");
           
            return response.Data;
        }
    }
}

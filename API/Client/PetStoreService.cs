using System.Text;
using System.Text.Json;
using Allure.Commons;
using PetStore.Entities;
using RestSharp;
using Serilog;

namespace PetStore.Client
{
    public class PetStoreService
    {
        public static RestClient Client { get; set; }

        public static RestResponse Response { get; set; }

        public static RestRequest SetRequest(string url, Method method)
        {
            Log.Information(@$"Request set for ""{url}"" with Method {method}");
            return new RestRequest(url, method);
        }

        public static RestResponse SendRequest(string url, Method method)
        {
            Response = Client.ExecuteAsync(SetRequest(url, method)).Result;
            Log.Information(@$"Send request for ""{url}"" with Method {method}");
            return Response;
        }

        public static RestResponse SendRequest(RestRequest request)
        {
            Response = Client.ExecuteAsync(request).Result;
            Log.Information(@$"Send request for ""{request}""");
            return Response;
        }

        public static async Task<Pet> SendRequestAnyc(RestRequest request)
        {
            var pet = (await Client.ExecuteAsync<Pet>(request)).Data;
            Log.Information(@$"Send request for ""{request}""");
            return pet;
        }

        public static Pet GetPetById(int id)
        {
            Log.Information($"Getting pet with id: {id}");
            var request = SetRequest("pet/{petId}", Method.Get);
            request.AddUrlSegment("petId", id);
            var conetent = SendRequest(request).Content;
            var returnedPet = JsonSerializer.Deserialize<Pet>(conetent);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(returnedPet, Newtonsoft.Json.Formatting.Indented);
            byte[] log_ = Encoding.ASCII.GetBytes(json);
            AllureLifecycle.Instance.AddAttachment("APIResponse", "application/json", log_, "json");
            var returnedPet2 = Client.ExecuteAsync<Pet>(request).Result.Content;
            Log.Information($"Got pet with id: {returnedPet.Id} and name: {returnedPet.Name}");
            return returnedPet;
        }

        public static Pet AddPet(Pet pet)
        {
            Log.Information($"Adding pet with id: {pet.Id} and name: {pet.Name}");
            var request = SetRequest("pet", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(pet);
            var returnedPet = JsonSerializer.Deserialize<Pet>(SendRequest(request).Content);
            Log.Information($"Added pet with id: {returnedPet.Id} and name: {returnedPet.Name}");
            return returnedPet;
        }

        public static async Task<Pet> AddPetAsync(Pet pet)
        {
            Log.Information($"Adding pet with id: {pet.Id} and name: {pet.Name} and thread id: ${Task.CurrentId}");
            var request = SetRequest("pet", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(pet);
            var returnedPet = await SendRequestAnyc(request);
            Log.Information($"Added pet with id: {returnedPet.Id} and name: {returnedPet.Name}");
            return returnedPet;
        }

        public static Pet AddPet()
        {
            Log.Information($"Adding pet with empty body");
            var request = SetRequest("pet", Method.Post);
            var returnedPet = JsonSerializer.Deserialize<Pet>(SendRequest(request).Content);
            Log.Information($"Added pet with id: {returnedPet.Id} and name: {returnedPet.Name}");
            return returnedPet;
        }

        public static Pet UpdatePet(Pet pet)
        {
            Log.Information($"Updating pet with id: {pet.Id} and name: {pet.Name}");
            var request = SetRequest("pet", Method.Put);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(pet);
            dynamic content = SendRequest(request).Content;
            var id = content.id;

            var returnedPet = JsonSerializer.Deserialize<Pet>(SendRequest(request).Content);
            Log.Information($"Updated pet id: {returnedPet.Id} and name: {returnedPet.Name}");
            return returnedPet;
        }

        public static Pet UpdatePet()
        {
            Log.Information($"Adding pet with empty body");
            var request = SetRequest("pet", Method.Put);
            var returnedPet = JsonSerializer.Deserialize<Pet>(SendRequest(request).Content);
            Log.Information($"Updated pet id: {returnedPet.Id} and name: {returnedPet.Name}");
            return returnedPet;
        }
    }
}

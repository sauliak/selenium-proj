﻿using Core.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDD.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        [BeforeScenario]
        public static void SetUp()
        {
           
        }

        [AfterScenario]
        public static void TearDown()
        {
             Browser.Instance.Close();
        }
    }
}

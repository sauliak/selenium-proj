using Core.Confing;
using Core.Selenium;
using selenuim.Pages;
using TechTalk.SpecFlow;

namespace BDD.StepDefinitions
{
    [Binding]
    public sealed class LoginStepDefinitions
    {
        private ScenarioContext _scenarioContext;

        public LoginStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given("I navigate to {string}")]
        public void GivenTheFirstNumberIs(string url)
        {
            Browser.Instance.Navigate(url);
            _scenarioContext.Add("HI", 1);
        }

        [Given("the following books")]
        public void GivenTheFirstNumberIs(Table url)
        {
           
        }

        [When("I log in with username (.*) and password (.*)")]
        public void GivenTheFirstNumberIs(string username, string password)
        {
            var loginPage = new LoginPage();
            loginPage.EnterUsername(username);
            loginPage.EnterPassword(password);
            loginPage.ClickLogin();
            var val = _scenarioContext.Get<int>("HI");
        }

        [Then("I should see product page")]
        public void ThenTheResultShouldBe()
        {
        }
    }
}
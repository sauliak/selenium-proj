﻿using Core.Enums;

namespace Core.Confing
{
    public class BrowserConfig
    {
        public string StartUrl { get; set; } 
        public bool HeadLess { get; set; }
        public BrowserType Type { get; set; }
    }
}

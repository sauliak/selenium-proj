﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Confing
{
    public class Configuration
    {
        private static readonly IConfigurationRoot ConfigurationRoot = GetConfiguration();

        private static IConfigurationRoot GetConfiguration()
        {

            var environmentName = Environment.GetEnvironmentVariable("TEST_ENV") ?? "DEV";
            return new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile($"config.{environmentName}.json", true, false)
               .Build();
        }

        public static BrowserConfig Browser = ConfigurationRoot.GetSection("Browser").Get<BrowserConfig>();

        public static string GetSetting(string key)
        {
            return ConfigurationRoot[key];
        }

        public static T GetGenericVal<T>(string key)
        {
            return ConfigurationRoot.GetValue<T>(key);
        }
    }
}

﻿using Core.Confing;
using Core.Selenium.BrowserFatory;
using OpenQA.Selenium;
using System.Text;
using System.Xml.Linq;

namespace Core.Selenium
{
    public class Browser
    {
        private static readonly ThreadLocal<Browser> BrowserInstances = new();
        public IWebDriver Driver { get; private set; }
        public static Browser Instance => BrowserInstances.Value ?? (BrowserInstances.Value = new Browser());

        private Browser()
        {
            Driver = DriverFactory.GetDriver();
            BrowserInstances.Value = this;
        }

        private static Browser GetBrowser()
        {
            return BrowserInstances.Value ?? (BrowserInstances.Value = new Browser());
        }

        public void Navigate(string url)
        {
            Driver.Navigate().GoToUrl(url);
        }

        public void ExecuteScript(string script, params string[] arguments)
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript(script, arguments);
        }

        public void ClearLocalStorage()
        {
            ExecuteScript("window.localStorage.clear();");
        }

        public void ClearSessionStorage()
        {
            ExecuteScript("window.sessionStorage.clear();");
        }

        public void Close()
        {
            Driver.Quit();
            Driver.Dispose();
            BrowserInstances.Value = null;
            Driver = null;
        }

        public void CloseWindow()
        {
            Driver.Close();
        }
    }
}

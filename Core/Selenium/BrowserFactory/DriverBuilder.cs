﻿using OpenQA.Selenium;

namespace Core.Selenium.BrowserFatory
{
    internal abstract class DriverBuilder
    {
        public abstract IWebDriver GetDriver();
    }
}

﻿using Core.Confing;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;

namespace Core.Selenium.BrowserFatory
{
    internal class ChromeDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.PageLoadStrategy = PageLoadStrategy.Eager;
            if (Configuration.Browser.HeadLess)
            {
                chromeOptions.AddArgument("--headless");
            }

            var chromeDriver = new ChromeDriver(chromeOptions);
            return chromeDriver;
            //var remoteUrl = Environment.GetEnvironmentVariable("SELENIUM_REMOTE_URL") ?? "http://localhost:4444/wd/hub";
            //return new RemoteWebDriver(new Uri(remoteUrl), chromeOptions);
        }
    }
}

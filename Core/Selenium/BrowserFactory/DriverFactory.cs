﻿using Core.Confing;
using Core.Enums;
using OpenQA.Selenium;

namespace Core.Selenium.BrowserFatory
{
    internal static class DriverFactory
    {
        public static readonly Dictionary<BrowserType, DriverBuilder> BrowserDictionary = new()
        {
            { BrowserType.Chrome, new ChromeDriverBuilder() }
        };

        public static IWebDriver GetDriver()
        {
            return BrowserDictionary[Configuration.Browser.Type].GetDriver();
        }
    }
}

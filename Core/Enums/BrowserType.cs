﻿namespace Core.Enums
{
    public enum BrowserType
    {
        Chrome,
        FireFox,
        Remote
    }
}

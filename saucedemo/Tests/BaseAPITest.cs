﻿using Core;
using Core.Confing;
using NUnit.Allure.Core;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using PetStore.Client;
using Serilog;
[assembly: Parallelizable(ParallelScope.Fixtures)]
[assembly: LevelOfParallelism(2)]

namespace Saucedemo.Tests
{
    [AllureNUnit]
    public class BaseAPITest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Log.Logger = Logger.Instance;
            var url = Configuration.GetSetting("API:PetStore:BaseUrl");
            PetStoreService.Client = new(new Uri(Configuration.GetSetting("API:PetStore:BaseUrl")));
            OneTimeSetup();
        }

        protected virtual void OneTimeSetup() { 
        }

        protected virtual void OneTimeTeardown()
        {
        }

        //[SetUp]
        //public void SetUp()
        //{
        //    Log.Information($"Starting new test: {TestContext.CurrentContext.Test.Name}");
        //}

        //[TearDown]
        //public void TearDown()
        //{
        //    if (TestContext.CurrentContext.Result.Outcome == ResultState.Success)
        //    {
        //        Log.Information($"Test {TestContext.CurrentContext.Test.Name} successfully passed!");
        //    }
        //    else
        //    {
        //        Log.Error($"Test {TestContext.CurrentContext.Test.Name} failed!");
        //    }
        //}

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            OneTimeTeardown();
            Log.CloseAndFlush();
            PetStoreService.Client.Dispose();
        }
    }
}

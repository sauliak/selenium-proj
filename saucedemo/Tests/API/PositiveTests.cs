using System.Diagnostics;
using NUnit.Allure.Attributes;
using NUnit.Framework;
using PetStore.Client;
using PetStore.Entities;
using PetStore.Entities.Builders;
using Saucedemo.Tests;

namespace PetStore.Tests.Positive
{
    [TestFixture]
    [AllureSuite("Smoke")]
    public class PositiveTests : BaseAPITest
    {
        [Test]
        public void GetPetByValidId()
        {
            var result = PetStoreService.GetPetById(1);
            Assert.Multiple(() =>
            {
                Assert.That(result.Id, Is.EqualTo(1));
                Assert.That((int)PetStoreService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(PetStoreService.Response.IsSuccessful, Is.True, $"Failed: {(int)PetStoreService.Response.StatusCode} : {PetStoreService.Response.StatusDescription}");
            });
        }

        [Test]
        public void UpdateExistingPetTest()
        {
            var pet = PetBuilder.Get();
            var result = PetStoreService.UpdatePet(pet);
            Assert.Multiple(() =>
            {
                Assert.That(PetStoreService.Response.IsSuccessful, Is.True, $"Failed: {(int)PetStoreService.Response.StatusCode} : {PetStoreService.Response.StatusDescription}");
                Assert.That((int)PetStoreService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.Name, Is.EqualTo(pet.Name));
            });
        }

        [Test]
        public void AddNewPetTest()
        {
            var pet = PetBuilder.Get();
            var result = PetStoreService.AddPet(pet);
            Assert.Multiple(() =>
            {
                Assert.That((int)PetStoreService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(PetStoreService.Response.IsSuccessful, Is.True, $"Failed: {(int)PetStoreService.Response.StatusCode} : {PetStoreService.Response.StatusDescription}");
                Assert.That(result.Name, Is.EqualTo(pet.Name));
            });
        }

        [Test]
        public void AddNewPetTestAsync()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            List<Task<Pet>> petTasks = new();
            for (int i = 0; i < 50; i++)
            {
                var pet = PetBuilder.Get();
                petTasks.Add(PetStoreService.AddPetAsync(pet));
            }
            var listOfpets = Task.WhenAll(petTasks).Result;

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            System.Console.WriteLine("List pet ids");
            foreach (var pet in listOfpets)
            {
                System.Console.WriteLine(pet.Id);
            }

            Stopwatch stopWatch1 = new Stopwatch();
            stopWatch1.Start();
            List<Pet> petList = new();
            for (int i = 0; i < 50; i++)
            {

                var pet = PetBuilder.Get();
                var result = PetStoreService.AddPet(pet);
                petList.Add(result);
            }
            stopWatch1.Stop();
            TimeSpan ts1 = stopWatch1.Elapsed;
            string elapsedTime1 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts1.Hours, ts1.Minutes, ts1.Seconds, ts1.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime1);






            //var pet = PetBuilder.Get();
            //var result = PetStoreService.AddPet(pet);
            //Assert.Multiple(() =>
            //{
            //    Assert.That((int)PetStoreService.Response.StatusCode, Is.EqualTo(200));
            //    Assert.That(PetStoreService.Response.IsSuccessful, Is.True, $"Failed: {(int)PetStoreService.Response.StatusCode} : {PetStoreService.Response.StatusDescription}");
            //    Assert.That(result.Name, Is.EqualTo(pet.Name));
            //});
        }
    }
}

﻿using Core.Confing;
using Core.Selenium;

namespace Saucedemo.Tests
{
    public class BaseUITest : BaseAPITest
    {
        protected Browser browser;

        protected override void OneTimeSetup()
        {
            browser = Browser.Instance;
        }

        protected override void OneTimeTeardown()
        {
            browser.Close();
        }
    }
}

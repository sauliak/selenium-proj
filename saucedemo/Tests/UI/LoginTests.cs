using Core.Confing;
using Core.Selenium;
using NUnit.Allure.Attributes;
using NUnit.Framework;
using OpenQA.Selenium;
using Saucedemo.Tests.Steps;
using selenuim.Pages.Steps;
using Saucedemo.Extensions;

namespace Saucedemo.Tests.UI
{
    [TestFixture]
    [AllureSuite("Smoke")]
    [Category("API")]
    internal class LoginTests : BaseUITest
    {
        ProductsSteps productsSteps;

        [SetUp]
        public void SetUp()
        {
            productsSteps = new ProductsSteps();
        }

        [Test, Category("test")]
        public void ShouldOpenPage()
        {
            var loginBtn = By.Id("signin");
            browser.Driver.Navigate().GoToUrl("http://uitestingplayground.com/visibility");
            var element = browser.Driver.FindElement(By.Id("invisibleButton"));
            element.TypeText("text");
        }

        [Test]
        public void ShouldLogInValidUserV1()
        {
            LoginSteps.LoginAs("standard_user", "secret_sauce");
            productsSteps.VerifyProductPageIsVisible();
        }
    }
}
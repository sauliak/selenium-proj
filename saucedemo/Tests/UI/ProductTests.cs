using NUnit.Allure.Attributes;
using NUnit.Framework;
using Saucedemo.DataProvider;
using Saucedemo.Tests.Steps;
using selenuim.Pages.Steps;

namespace Saucedemo.Tests.UI
{
    //[TestFixture]
    //[AllureSuite("Smoke")]
    internal class ProductTests 
    {
        ProductsSteps productsSteps;
        [SetUp]
        public void SetUp()
        {
            productsSteps = new ProductsSteps();
        }

        [Test]
        public void ShouldCountSeletedProductdCorrectrly()
        {
            int expectedNumberOfselectedProducts = 2;
            string[] productNames = { "Sauce Labs Backpack", "Sauce Labs Bike Light" };
            LoginSteps.LoginAs("standard_user", "secret_sauce");
            productsSteps.SelectProductsByName(productNames);
            productsSteps.VerifyProductCounter(2);
        }

        public void ShouldGetToFinalCheckoutStep()
        {
            var checkoutData = SauceDemoDataGenerator.GetRandomCheckoutEntity();
            string[] productNames = { "Sauce Labs Backpack", "Sauce Labs Bike Light" };
            LoginSteps.LoginAs("standard_user", "secret_sauce");
            productsSteps.SelectProductsByName(productNames);
            productsSteps.OpenShoppingCart();
            productsSteps.Checkout();
            productsSteps.FillCheckOutInfo(checkoutData);
            productsSteps.Continue();
        }
    }
}

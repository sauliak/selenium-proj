﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Saucedemo.Pages
{
    internal class CheckoutPage : BasePage
    {
        private readonly string _url = "https://www.saucedemo.com/checkout-step-one.html";

        [FindsBy(How = How.Id, Using = "first-name")]
        private IWebElement FirstNameInput;

        [FindsBy(How = How.Id, Using = "last-name")]
        private IWebElement LastNameInput;

        [FindsBy(How = How.Id, Using = "postal-code")]
        public IWebElement PostCodeInput;

        [FindsBy(How = How.Id, Using = "continue")]
        public IWebElement ContinueButton;

        [FindsBy(How = How.Id, Using = "cancel")]
        public IWebElement CancelButton;

        public CheckoutPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public void SetFirstName(string firstName)
        {
            FirstNameInput.SendKeys(firstName);
        }

        public void SetLastName(string lastName)
        {
            LastNameInput.SendKeys(lastName);
        }

        public void SetZipCode(string zipCode)
        {
            PostCodeInput.SendKeys(zipCode);
        }

        public void Continue()
        {
            ContinueButton.Click();
        }

        public void Cancel()
        {
            CancelButton.Click();
        }
    }
}

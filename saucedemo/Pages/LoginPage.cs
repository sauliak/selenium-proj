﻿using OpenQA.Selenium;
using Saucedemo.Pages;
using SeleniumExtras.PageObjects;

namespace selenuim.Pages
{
    public class LoginPage : BasePage
    {
        private readonly string _url = "https://www.saucedemo.com/";
        
        [FindsBy(How = How.Id, Using = "user-name")]
        private IWebElement UsernameField;
        
        [FindsBy(How = How.Id, Using = "password")]
        private IWebElement PasswordField;

        [FindsBy(How = How.Id, Using = "login-button")]
        public IWebElement LoginButton;

        [FindsBy(How = How.CssSelector, Using = ".error-message-container.error")]
        private IWebElement ErrorMessage;

        public LoginPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public LoginPage EnterUsername(string username)
        {
            UsernameField.SendKeys(username);
            return this;
        }

        public LoginPage EnterPassword(string password)
        {
            PasswordField.SendKeys(password);
            return this;
        }

        public ProductsPage ClickLogin()
        {
            LoginButton.Click();
            return new ProductsPage();
        }

        public bool IsErrorMessageDisplayed()
        {
            return ErrorMessage.Displayed;
        }

        public string GetErrorMessage()
        {
            return ErrorMessage.Text;
        }

        public void Login(string username, string password)
        {
            EnterUsername(username);
            EnterPassword(password);
            ClickLogin();
        }
    }
}

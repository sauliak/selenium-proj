﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Saucedemo.Pages
{
    internal class CartPage : BasePage
    {
        private readonly string _url = "https://www.saucedemo.com/cart.html";


        [FindsBy(How = How.Id, Using = "continue-shopping")]
        public IWebElement ContinueShopingButton;

        [FindsBy(How = How.Id, Using = "checkout")]
        public IWebElement CheckoutButton;


        public CartPage()
        {
            PageFactory.InitElements(driver, this);
        }


        public void ContinueShopping()
        {
            ContinueShopingButton.Click();
        }

        public void Checkout()
        {
            CheckoutButton.Click();
        }
    }
}

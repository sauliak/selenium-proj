﻿using OpenQA.Selenium;
using Core.Selenium;
using OpenQA.Selenium.Support.Events;
using Serilog;
using NUnit.Framework;

namespace Saucedemo.Pages
{
    public class BasePage
    {
        protected IWebDriver driver;
        protected static ILogger Logger = Core.Logger.Instance;

        public BasePage()
        {
            var firingDriver = new EventFiringWebDriver(Browser.Instance.Driver);

            firingDriver.ExceptionThrown +=
         new EventHandler<WebDriverExceptionEventArgs>(firingDriver_ExceptionThrown);
            firingDriver.ElementClicked +=
                new EventHandler<WebElementEventArgs>(firingDriver_ElementClicked);
            firingDriver.FindElementCompleted +=
                new EventHandler<FindElementEventArgs>(firingDriver_FindElementCompleted);

            driver = firingDriver;
        }

        static void firingDriver_ExceptionThrown(object sender, WebDriverExceptionEventArgs e)
        {
            Console.WriteLine(e.ThrownException.Message);
        }

        static void firingDriver_ElementClicked(object sender, WebElementEventArgs e)
        {
            Logger.Information("from event handler" + e.Element.ToString());
            TestContext.Progress.WriteLine("from event handler" + e.Element.ToString());
        }

        static void firingDriver_FindElementCompleted(object sender, FindElementEventArgs e)
        {
            Logger.Information("from event handler" + e.FindMethod.ToString());
            TestContext.Progress.WriteLine("from event handler" + e.FindMethod.ToString());
        }
    }
}

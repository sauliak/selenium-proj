﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Saucedemo.Pages;
using SeleniumExtras.PageObjects;
using selenuim.WebParts;

namespace selenuim.Pages
{
    public class ProductsPage : BasePage
    {
        [FindsBy(How = How.Id, Using = "shopping_cart_container")]
        private IWebElement ShoppingCard;

        [FindsBy(How = How.Id, Using = "react-burger-menu-btn")]
        private IWebElement MenuButton;

        [FindsBy(How = How.ClassName, Using = "shopping_cart_badge")]
        private IWebElement ShoppingCounter;

        // Locator for an element that is only visible after a successful login
        private IWebElement InventoryContainer => driver.FindElement(By.Id("inventory_container"));

        public ProductsPage()
        {
            
            PageFactory.InitElements(driver, this);
        }

        public string GetShoppingCounterValue()
        {
            return ShoppingCounter.Text;
        }

        public bool IsInventoryVisible()
        {
            return InventoryContainer.Displayed;
        }

        public IWebElement GetProductByName(string name)
        {
            return driver.FindElement(By.XPath($"//div[@class='inventory_item' and .//*[contains(text(),'{name}')]]"));
        }

        public IWebElement GetSelectButtonByName(string name)
        {
            return GetProductByName(name).FindElement(By.XPath(".//button"));
        }

        public void ClickShppingCardButton()
        {
            ShoppingCard.Click();
        }

        public LeftMenuWebPart OpenLeftMenu()
        {
            MenuButton.Click();
            return new LeftMenuWebPart();
        }
    }
}

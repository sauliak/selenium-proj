﻿using OpenQA.Selenium;
using Saucedemo.Pages;
using SeleniumExtras.PageObjects;
using selenuim.Pages;

namespace selenuim.WebParts
{
    public class LeftMenuWebPart : BasePage
    {
        [FindsBy(How = How.Id, Using = "inventory_sidebar_link")]
        private IWebElement AllItems;

        [FindsBy(How = How.Id, Using = "about_sidebar_link")]
        private IWebElement About;

        [FindsBy(How = How.Id, Using = "logout_sidebar_link")]
        private IWebElement LogOut;

        [FindsBy(How = How.Id, Using = "reset_sidebar_link")]
        private IWebElement ResetPage;

        [FindsBy(How = How.Id, Using = "react-burger-cross-btn")]
        private IWebElement CloseMenuBtn;

        public LeftMenuWebPart()
        {
            PageFactory.InitElements(driver, this);
        }

        public LoginPage ClickLogOut()
        {
            LogOut.Click();
            return new LoginPage();
        }
    }
}

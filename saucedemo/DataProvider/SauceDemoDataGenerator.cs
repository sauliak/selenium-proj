﻿using Saucedemo.Models;

namespace Saucedemo.DataProvider
{
    internal class SauceDemoDataGenerator
    {
        public static CheckoutEntity GetRandomCheckoutEntity()
        {
            return new CheckoutEntity()
            {
                FirstName = Faker.Name.First(),
                LastName = Faker.Name.Last(),
                ZipCode = Faker.Address.ZipCode()
            };
        }
    }
}

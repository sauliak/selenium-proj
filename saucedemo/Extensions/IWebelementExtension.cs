﻿using Core.Selenium;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Globalization;

namespace Saucedemo.Extensions
{
    public static class IWebelementExtension
    {
        public static void TypeText(this IWebElement source, string text)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            source.WaitUntil(element => !element.Enabled, TimeSpan.FromSeconds(5));
            source.Click();
            source.Clear();
            source.SendKeys(text);
        }


        private static void ThrowTimeoutException(string exceptionMessage, Exception lastException)
        {
            throw new WebDriverTimeoutException(exceptionMessage, lastException);
        }

        private static TimeSpan DefaultSleepTimeout
        {
            get { return TimeSpan.FromMilliseconds(500); }
        }


        public static void WaitUntil(this IWebElement input, Func<IWebElement, bool> condition, TimeSpan? time = null)
        {
            TimeSpan timeout = time ?? DefaultSleepTimeout;
            var sleepInterval = DefaultSleepTimeout;
            IClock clock = new SystemClock();
            if (condition == null)
            {
                throw new ArgumentNullException(nameof(condition), "condition cannot be null");
            }

            //var resultType = typeof(TResult);
            //if ((resultType.IsValueType && resultType != typeof(bool)) || !typeof(object).IsAssignableFrom(resultType))
            //{
            //    throw new ArgumentException("Can only wait on an object or boolean response, tried to use type: " + resultType.ToString(), nameof(condition));
            //}

            Exception lastException = null;
            var endTime = clock.LaterBy(timeout);
            while (true)
            {

                var result = condition(input); 
                if (result) { return; }


                // Check the timeout after evaluating the function to ensure conditions
                // with a zero timeout can succeed.
                if (!clock.IsNowBefore(endTime))
                {
                    //string timeoutMessage = string.Format(CultureInfo.InvariantCulture, "Timed out after {0} seconds", this.timeout.TotalSeconds);
                    //if (!string.IsNullOrEmpty(this.message))
                    //{
                    //    timeoutMessage += ": " + this.message;
                    //}


                    ThrowTimeoutException("text", lastException);
                }


                Thread.Sleep(sleepInterval);
            }

        }
    }
}
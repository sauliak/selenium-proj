﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Selenium;
using Core.Selenium.WebDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace Saucedemo.UI
{
    public class ElementCollection<T> : IEnumerable<T> where T : Element
    {
        IWebDriver driver;
        public ElementCollection(string locator, Element parent = null)
        {
            Parent = parent;
            Locator = locator;
            driver = Browser.Instance.Driver;
        }

        protected LocatorHandler Lh = new LocatorHandler();
        

        protected Element Parent { get; set; }
        protected string Locator { get; set; }
        
        private ISearchContext GetSearchContext()
        {
            if (Parent != null)
                return Parent.WrappedElement;
            return driver;
        }

        protected IEnumerable<T> _collection;
        protected IEnumerable<T> Collection
        {
            get
            {
                if (_collection == null)
                    _collection = GetSearchContext().FindElements(Lh.GetSeleniumBy(Locator)).Select(el =>
                    {
                        var item = ((T) Activator.CreateInstance(typeof (T), (string) null, Parent));
                        item.WrappedElement = el;
                        return item;
                    });
                return _collection;
            }
        }

    

        public IEnumerator<T> GetEnumerator()
        {
            return Collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ElementCollection : ElementCollection<Element>
    {
        public ElementCollection(string locator, Element parent = null) : base(locator, parent)
        {
        }
    }
}

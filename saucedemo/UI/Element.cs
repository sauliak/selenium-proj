﻿using System;
using System.Linq;
using Core.Selenium;
using Core.Selenium.WebDriver;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using Saucedemo.UI;
using Saucedemo.UI.Exceptions;

namespace Saucedemo.UI
{
    public class Element
    {
        IWebDriver driver;
        public Element(string locator, Element parent = null)
        {
            Parent = parent;
            Locator = locator;
            driver = Browser.Instance.Driver;
        }


        protected LocatorHandler Lh = new LocatorHandler();
        protected Element Parent { get; set; }
        public string Locator { get; set; }

        protected internal bool IsValidStatement = true;

        private ISearchContext GetSearchContext()
        {
            if (Parent != null)
            {
                Parent.WaitForToBePresent();
                return Parent.WrappedElement;   
            }
            return Browser.Instance.Driver;
        }

        private IWebElement _wrappedElement;
        protected internal IWebElement WrappedElement
        {
            get 
            {
                if (_wrappedElement == null)
                {
                    _wrappedElement = GetSearchContext().FindElement(Lh.GetSeleniumBy(Locator));
                }
                return _wrappedElement;
            }
            set { _wrappedElement = value; }
        }

        #region Find elements

        public T FindElement<T>(string locator) where T : Element
        {
            return (T)Activator.CreateInstance(typeof (T), locator, this);
        }

        public ElementCollection<T> FindElements<T>(string locator) where T : Element
        {
            return new ElementCollection<T>(locator, this);
        }

        public Element FindElement(string locator)
        {
            return FindElement<Element>(locator);
        }

        public ElementCollection FindElements(string locator)
        {
            return new ElementCollection(locator, this);
        }

        #endregion

        public Boolean IsPresent()
        {
            try
            {
                if (GetSearchContext().FindElements(Lh.GetSeleniumBy(Locator)).Any())
                {
                    return true;
                }
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
            return false;
        }

        public Boolean IsVisible()
        {
            try
            {
                if (WrappedElement.Displayed)
                {
                    return true;
                }
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
            return false;
        }

        #region Waits
        public bool WaitForToBePresent(TimeSpan timeout = default(TimeSpan))
        {
            timeout = timeout.Equals(default(TimeSpan)) ? TimeSpan.FromMinutes(3) : timeout;
            WebDriverWait wait = new WebDriverWait(Browser.Instance.Driver, timeout);
            // this doesn't work???
            // wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            wait.PollingInterval = TimeSpan.FromSeconds(1);

            try
            {
                return wait.Until(dr =>
                {
                    try
                    {
                        return WrappedElement != null;
                    }

                    catch (NoSuchElementException e)
                    {
                        Core.Logger.Instance.Information(e.Message);
                        return false;
                    }
                    catch (StaleElementReferenceException e)
                    {
                        Core.Logger.Instance.Information(e.Message);
                        return false;
                    }
                });
            }
            catch (TimeoutException e)
            {
                Core.Logger.Instance.Error(e.Message);
                throw new ElementWaitTimeoutException("[Element] Element located by [" + Locator +
                                                      "] is NOT present after 10 second(s) expectation.");
            }
        }

        /*public Boolean WaitForToBeNotPresent(TimeSpan timeout = default(TimeSpan))
        {
            timeout = timeout.Equals(default(TimeSpan)) ? TimeSpan.FromSeconds(10) : timeout;
            WebDriverWait wait = new WebDriverWait(Runner.Driver, timeout);
            wait.PollingInterval = TimeSpan.FromSeconds(1);

            return wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(Lh.GetSeleniumBy(Locator))) == null;
        }*/

        public Boolean WaitForToBeVisible(TimeSpan timeout = default(TimeSpan))
        {
            timeout = timeout.Equals(default(TimeSpan)) ? TimeSpan.FromMinutes(3) : timeout;
            WebDriverWait wait = new WebDriverWait(Browser.Instance.Driver, timeout);
            // this doesn't work???
//            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            wait.PollingInterval = TimeSpan.FromSeconds(1);

            try
            {
                return wait.Until(dr =>
                {
                    try
                    {
                        return WrappedElement != null;
                    }

                    catch (NoSuchElementException e)
                    {
                        Core.Logger.Instance.Information(e.Message);
                        return false;
                    }
                    catch (StaleElementReferenceException e)
                    {
                        Core.Logger.Instance.Information(e.Message);
                        return false;
                    }
                });
            }
            catch (TimeoutException e)
            {
                Core.Logger.Instance.Error(e.Message);
                throw new ElementWaitTimeoutException("[Element] Element located by [" + Locator + "] is NOT visible after 10 second(s) expectation.");
            }
        }
/*
        public Boolean WaitForToBeNotVisible(TimeSpan timeout = default(TimeSpan))
        {
            timeout = timeout.Equals(default(TimeSpan)) ? TimeSpan.FromSeconds(10) : timeout;
            WebDriverWait wait = new WebDriverWait(Runner.Driver, timeout);
            wait.PollingInterval = TimeSpan.FromSeconds(1);

            return wait.Until(ExpectedConditions.InvisibilityOfElementLocated(Lh.GetSeleniumBy(Locator)));
        }*/



        public Boolean WaitForToBeDisplayed(TimeSpan timeout = default(TimeSpan))
        {
            return WaitForToBePresent(timeout) && WaitForToBeVisible(timeout);
        }
        /*
        public Boolean WaitForToBeNotDisplayed(TimeSpan timeout = default(TimeSpan))
        {
            return WaitForToBeNotPresent(timeout) && WaitForToBeNotVisible(timeout);
        }
*/

        #endregion


        public void Click()
        {
            WaitForToBeDisplayed();
            WrappedElement.Click();
//            throw new InvalidOperationException("[Element] Element [" + Locator + "] is NOT present in DOM or is NOT visible!");
        }

        public void DoubleClick()
        {
            Actions builder = new Actions(driver);
            builder.DoubleClick(WrappedElement).Build().Perform();
        }

        public void MultipleClick()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
            for (int i = 0; i < 3; i++)
            {
                Click();
            }
        }

        #region Actions
        public string GetValue()
        {
            string value = WrappedElement.GetAttribute("value");
            return value;
        }

        public virtual void SetValue(string value)
        {
            WaitForToBeDisplayed();
            WrappedElement.Clear();
            WrappedElement.SendKeys(value);
        }

        public string GetAttribute(string attribute)
        {
            return WrappedElement.GetAttribute(attribute);
        }

        public string GetCss(string name)
        {
            return WrappedElement.GetCssValue(name);
        }
        #endregion

        #region Properties
        public string TagName
        {
            get { return WrappedElement.TagName; }
        }

        public string Text
        {
            get { return WrappedElement.Text; }
        }

        /// <summary>
        /// Use this method, if element is not visible
        /// </summary>
        public string InnerText
        {
            get { return driver.ExecuteJavaScript<string>("return arguments[0].innerHTML", this); }
        }

        public bool Enabled
        {
            get { return WrappedElement.Enabled; }
        }

        #endregion
    }
}

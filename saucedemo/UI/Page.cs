﻿using System;
using Core;
using Core.Selenium;

using OpenQA.Selenium;
using Saucedemo.Tests.UI.Utils;
using Saucedemo.UI.Exceptions;
using Saucedemo.UI.Utils;

namespace Saucedemo.UI
{
    public class Page : Block
    {
        public Page() : base("//body", null)
        {
            InitializeFields();
        }

        /** 
         * Page interaction methods
         */

        public String GetTitle()
        {
            return Browser.Instance.Driver.Title;
        }

        public String GetUrl()
        {
            return Browser.Instance.Driver.Url;
        }
        //	
        //	public <T> T getUIControl(Class<T> returnPage)  {
        //		return returnPage.newInstance();
        //	}
        //
        //    public <T> T getUIControl(String frameName, Class<T> returnPage) throws InstantiationException, IllegalAccessException {
        //        switchToDefaultContent();
        //        switchToFrame(frameName);
        //        return returnPage.newInstance();
        //    }

        public void Refresh()
        {
            Logger.Instance.Information("[Page] Refresh page.");
            Browser.Instance.Driver.Navigate().Refresh();
        }

        public Boolean ContainsText(String text)
        {
            if (Browser.Instance.Driver.PageSource.Contains(text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public T WaitForPageToLoad<T>() where T : Page
        {
            Browser.Instance.WaitFor(Condition.PageLoad);
            return this as T;
        }

//        public void WaitForPageToLoad(int timeInSeconds)
//        {
//            Runner.Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(timeInSeconds));
//        }
//
//        public void WaitForTimeout(TimeSpan timeout)
//        {
//            Thread.Sleep(timeout);
//        }


        #region Frames, windows
        public void SelectWindow(String iframeName)
        {
            Browser.Instance.Driver.SwitchTo().Window(iframeName);
        }

        public void SwitchToFrame(Element iframe)
        {
            Browser.Instance.Driver.SwitchTo().Frame(iframe.Locator);
        }

        public void SwitchToFrame(int iframeIndex)
        {
            Browser.Instance.Driver.SwitchTo().Frame(iframeIndex);
        }

        public void SwitchToFrame(string iframeName)
        {
            Logger.Instance.Debug("[Page] Switch interaction to frame [" + iframeName + "].");
            try
            {
                Browser.Instance.Driver.SwitchTo().Frame(iframeName);
            }
            catch (NoSuchFrameException e)
            {
                Logger.Instance.Error(e.Message);
                throw new PageContentNotFoundException("[Page] Iframe block with name [" + iframeName + "] NOT found!");
            }

        }

        public void SwitchToDefaultContent()
        {
            Logger.Instance.Debug("[Page] Switch interaction to default content.");
            Browser.Instance.Driver.SwitchTo().DefaultContent();
        }
        #endregion
        



    }
}

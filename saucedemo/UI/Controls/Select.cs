﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Saucedemo.UI.Utils;

namespace Saucedemo.UI.Controls
{
    public class Select : Element
    {
        public Select(string locator, Element parent = null) : base(locator, parent)
        {
        }

        public void SelectValue(string item)
        {
            var optionToSelect = FindElements(".//option").FirstOrDefault(opt => opt.HasText(item));
            if (optionToSelect == null)
                throw new Exceptions.CommonException(string.Format("Option with value '{0}' is not found", item));
            optionToSelect.Click();
        }
    }
}

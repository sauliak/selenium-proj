﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NetTAF.Core.Attributes;
using Saucedemo.Tests.UI.Utils;
using Saucedemo.UI.Utils;

namespace Saucedemo.UI
{
    public class Block : Element
    {
        public Block(string locator, Element parent = null) : base(locator, parent)
        {
        }
        
        protected void InitializeFields()
        {
            var findByType = typeof (FindByAttribute);
            var elementType = typeof (Element);
            var collectionType = typeof (ElementCollection);
            var genericCollectionType = typeof (ElementCollection<>);

            var fields = GetType()
                .GetFields(BindingFlags.Public | BindingFlags.Instance);
            var fieldsToInitialize = fields
                    .Where(prop =>
                        (
                            elementType.IsAssignableFrom(prop.FieldType) 
                            || collectionType.IsAssignableFrom(prop.FieldType) 
                            || (prop.FieldType.IsGenericType && prop.FieldType.GetGenericTypeDefinition() == genericCollectionType)
                        ) 
                        && prop.GetCustomAttributes(findByType).Any());


            foreach (var field in fieldsToInitialize)
            {
                var findByAttribute = (FindByAttribute)field.GetCustomAttributes(findByType).First();
                var locator = findByAttribute.Locator;
                
                var actualFieldType = field.FieldType;

                // Important: we use block as parent
                if (elementType.IsAssignableFrom(actualFieldType))
                {
                    field.SetValue(this, Activator.CreateInstance(actualFieldType, locator, (Element) this));
                    continue;
                }

                if (collectionType.IsAssignableFrom(actualFieldType))
                {
                    field.SetValue(this, Activator.CreateInstance(actualFieldType, locator, (Element)this));
                    continue;
                }

                if (actualFieldType.IsGenericType && actualFieldType.GetGenericTypeDefinition() == genericCollectionType)
                {
                    field.SetValue(this, Activator.CreateInstance(actualFieldType, locator, (Element)this));
                }
            }
        }

        public void WaitLoadIndicatorsFields()
        {
            var loadIndicatorType = typeof (LoadIndicatorAttribute);
            var elementType = typeof (Element);
            var collectionType = typeof(ElementCollection);
            var genericCollectionType = typeof (ElementCollection<>);

            var fieldsToWait = 
                GetType()
                .GetFields(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => (
                    elementType.IsAssignableFrom(prop.FieldType)
                            || collectionType.IsAssignableFrom(prop.FieldType)
                            || (prop.FieldType.IsGenericType && prop.FieldType.GetGenericTypeDefinition() == genericCollectionType)
                    ) 
                    && prop.GetCustomAttributes(loadIndicatorType).Any());

            foreach (var field in fieldsToWait)
            {
                var actualFieldType = field.FieldType;
                var loadIndicatorAttribute = (LoadIndicatorAttribute)field.GetCustomAttributes(loadIndicatorType).First();

                if (elementType.IsAssignableFrom(actualFieldType))
                {
                    
                    var actualField = (Element)field.GetValue(this);
                    if (loadIndicatorAttribute.WaitVisible)
                    {
                        actualField.WaitFor(Condition.Visible);
                    }
                    else
                    {
                        actualField.WaitFor(el => el.IsPresent());
                    }
                    
                    continue;
                }

                if (collectionType.IsAssignableFrom(actualFieldType))
                {
                    
                    var actualField = (IEnumerable<Element>)field.GetValue(this);
                    if (loadIndicatorAttribute.WaitVisible)
                    {
                        actualField.WaitForVisible();
                    }
                    else
                    {
                        actualField.WaitForPresent();
                    }
                    continue;
                }

                if (collectionType.IsAssignableFrom(actualFieldType) 
                    || (actualFieldType.IsGenericType && actualFieldType.GetGenericTypeDefinition() == genericCollectionType))
                {
                    var actualField = (IEnumerable<Element>)field.GetValue(this);
                    if (loadIndicatorAttribute.WaitVisible)
                    {
                        actualField.WaitForVisible();
                    }
                    else
                    {
                        actualField.WaitForPresent();
                    }
                }
            }
        }
    }
}

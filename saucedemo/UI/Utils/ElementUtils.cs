﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Selenium;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using Saucedemo.Tests.UI.Utils;
using Saucedemo.UI.Controls;

namespace Saucedemo.UI.Utils
{
    public static class ElementUtils
    {
        public static T Should<T>(this T element, Predicate<T> condition, string message = null, params object[] args) where T : Element
        {
            return ShouldBe(element, condition, message, args);
        }

        public static T ShouldBe<T>(this T element, Predicate<T> condition, string message = null, params object[] args) where T : Element
        {
            Assert.IsTrue(condition(element), message, args);
            return element;
        }

        public static T ShouldHaveText<T>(this T element, string text) where T : Element
        {
            var actualText = element.Text;
            
            Assert.IsTrue(text.Trim().Equals(actualText.Trim()), "Wrong element text. Expected:\n{0}\nActual:\n{1}", text, actualText);

            return element;
        }

        public static T ShouldContainText<T>(this T element, string text) where T : Element
        {
            var actualText = element.Text;
            Assert.IsTrue(actualText.Contains(text), "Wrong element text. Expected contains:\n{0}\nActual:\n{1}", text, actualText);

            return element;
        }
        
        
        public static T WaitFor<T>(this T element, Predicate<T> condition, TimeSpan timeout = default(TimeSpan), string message = null) where T : Element
        {
            // TODO: get rid of WD. Add message
            if (timeout.Equals(default(TimeSpan)))
                timeout = TimeSpan.FromMinutes(3);

            var wait = new WebDriverWait(Browser.Instance.Driver, timeout);
            wait.Until(driver => condition(element));

            return element;
        }
        
        public static bool HasText(this Element element, string text)
        {
            return element.Text.Trim().Equals(text.Trim());
        }

        public static bool ContainsText(this Element element, string text)
        {
            return element.Text.Contains(text);
        }

       
        public static string PrepareLocator(string locatorTemplate, params object[] args)
        {
            return string.Format(locatorTemplate, args);
        }

        public static void FillForm(Dictionary<Element, object> valuesToFill)
        {
            foreach (var pairToFill in valuesToFill)
            {
                if (pairToFill.Value == null)
                    continue;

                FillValue(pairToFill.Key, pairToFill.Value);
            }
        }

        private static void FillValue(Element element, object value)
        {
            if (element is Checkbox && value is bool?)
            {
                (element as Checkbox).SetState((value as bool?).Value);
            }
            else if (element is Select)
            {
                (element as Select).SelectValue(value.ToString());
            }

            else
            {
                element.SetValue(value.ToString());
            }
        }
        
    }
}
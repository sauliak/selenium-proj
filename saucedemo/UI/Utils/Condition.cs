﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Selenium;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Saucedemo.UI;

namespace Saucedemo.Tests.UI.Utils
{
    public static class Condition
    {
        #region Element conditions
        public static Predicate<Element> Visible = element => element.IsVisible();

        public static Predicate<Element> Invisible = element => !element.IsVisible();

        public static Predicate<Element> Disabled = element => !element.Enabled;

        public static Predicate<Element> Enabled = element => element.Enabled;

        #endregion

        #region Browser condition

        public static Predicate<Browser> PageLoad = browser =>
        {
            var dr = Browser.Instance.Driver;
            bool result = false;
            try
            {
                dr.SwitchTo().Window(dr.CurrentWindowHandle);
                result = ((IJavaScriptExecutor)dr).ExecuteScript("return (typeof($) === 'undefined') ? true : !$.active;").Equals(true);
            }
            catch (NoSuchWindowException)
            {
                dr.SwitchTo().Window(dr.WindowHandles.First());
            }
            return result;
        };

        #endregion

        #region Collection conditions

        #endregion

    }
}

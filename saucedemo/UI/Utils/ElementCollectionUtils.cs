﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using Saucedemo.Tests.UI.Utils;

namespace Saucedemo.UI.Utils
{
    public static class ElementCollectionUtils
    {
        // Just example
        /*public static IEnumerable<T> Visible<T>(this IEnumerable<T> collection) where T : Element
        {
            
            return collection;
        }*/

        /// <summary>
        /// Important: return filtered collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="condition"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IEnumerable<T> AnyShouldBe<T>(this IEnumerable<T> collection, Predicate<T> condition, string message = null, params object[] args) where T : Element
        {
            return ShouldBe(collection, condition, message, args);
        }

        public static IEnumerable<T> ShouldBe<T>(this IEnumerable<T> collection, Predicate<T> condition, string message = null, params object[] args) where T : Element
        {
            var filtered = collection.Where(el => condition(el));
            Assert.IsTrue(filtered.Any(), message, args);
            return filtered;
        }

        public static IEnumerable<T> WaitForAny<T>(this IEnumerable<T> collection, Predicate<T> condition, TimeSpan timeout = default(TimeSpan), string message = null) where T : Element
        {
            if (timeout.Equals(default(TimeSpan)))
                timeout = TimeSpan.FromSeconds(30);

            var wait = new WebDriverWait(Browser.Instance.Driver, timeout);
            wait.Until(driver => collection.Any(el => condition(el)));

            return collection;
        }

        public static IEnumerable<T> WaitForPresent<T>(this IEnumerable<T> collection, TimeSpan timeout = default(TimeSpan), string message = null) where T : Element
        {
            return WaitForAny<T>(collection, Condition.Visible, timeout, message);
        }

        public static IEnumerable<T> WaitForVisible<T>(this IEnumerable<T> collection, TimeSpan timeout = default(TimeSpan), string message = null) where T : Element
        {
            return WaitForAny<T>(collection, Condition.Visible, timeout, message);
        }
    }
}

﻿using System;
using Core.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Saucedemo.UI.Utils
{
    public static class BrowserUtils
    {
        public static Browser WaitFor(this Browser browser, Predicate<Browser> condition, TimeSpan timeout = default(TimeSpan), string message = null) 
        {
            if (timeout.Equals(default(TimeSpan)))
                timeout = TimeSpan.FromMinutes(10);

            // TODO: get rid of WD. Add message
            var wait = new WebDriverWait(browser.Driver, timeout);
            wait.IgnoreExceptionTypes(typeof(InvalidOperationException));
            wait.Until(driver => condition(browser));

            return browser;
        }
    }
}
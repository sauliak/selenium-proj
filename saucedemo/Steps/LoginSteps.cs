﻿using OpenQA.Selenium;
using Saucedemo.Models;
using selenuim.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saucedemo.Tests.Steps
{
    internal class LoginSteps
    {
        public static ProductsPage LoginAs(string username, string password)
        {
            var loginPage= new LoginPage();
            loginPage.EnterUsername(username);
            loginPage.EnterPassword(password);
            loginPage.ClickLogin();
            return new ProductsPage();
        }

        public static ProductsPage LoginAs(User user)
        {
            var loginPage = new LoginPage();
            loginPage.EnterUsername(user.Name);
            loginPage.EnterPassword(user.Password);
            loginPage.ClickLogin();
            return new ProductsPage();
        }

        public bool IsOpen()
        {
            var loginPage = new LoginPage();
            return loginPage.LoginButton.Displayed;
        }
    }
}

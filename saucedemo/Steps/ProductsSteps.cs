﻿using NUnit.Framework;
using Saucedemo.Models;
using Saucedemo.Pages;

namespace selenuim.Pages.Steps
{
    internal class ProductsSteps
    {
        private readonly ProductsPage productsPage;
        private readonly CartPage cartPage;
        private readonly CheckoutPage checkoutPage;


        public ProductsSteps()
        {
            cartPage = new CartPage();
            checkoutPage = new CheckoutPage();
            productsPage = new ProductsPage();
        }

        public void SelectProductsByNameAndGoToCard(params string[] productNames)
        {
            SelectProductsByName(productNames);
            productsPage.ClickShppingCardButton();
        }

        public void SelectProductsByName(params string[] productNames)
        {
            foreach (var productName in productNames)
            {
                var selectButton = productsPage.GetSelectButtonByName(productName);
                selectButton.Click();
            }
        }

        public void VerifyProductCounter(int expectedCount)
        {
            var actualCount = productsPage.GetShoppingCounterValue();
            Assert.That(actualCount, Is.EqualTo(expectedCount.ToString()), "Counter is wrong");
        }

        public void Logout()
        {
            var menu = productsPage.OpenLeftMenu();
            menu.ClickLogOut();
        }

        public void VerifyProductPageIsVisible()
        {
            Assert.IsTrue(productsPage.IsInventoryVisible(), "Inventory should be visible after successful login.");
        }

        public static void SelectProducsByNameAndContinue(params string[] productNames)
        {
            var productsPage = new ProductsPage();
            foreach (var productName in productNames)
            {
                var element = productsPage.GetSelectButtonByName(productName);
                element.Click();
            }
            productsPage.ClickShppingCardButton();
        }

        public static void Logut()
        {
            var productsPage = new ProductsPage();
            var menu = productsPage.OpenLeftMenu();
            menu.ClickLogOut();
        }

        public void OpenShoppingCart()
        {
            productsPage.ClickShppingCardButton();
        }

        public void ContinueShopping()
        {
            cartPage.ContinueShopping();
        }

        public void Checkout()
        {
            cartPage.Checkout();
        }

        public void Continue()
        {
            checkoutPage.Continue();
        }

        public void FillCheckOutInfo(CheckoutEntity checkoutEntity)
        {
            checkoutPage.SetFirstName(checkoutEntity.FirstName);
            checkoutPage.SetLastName(checkoutEntity.LastName);
            checkoutPage.SetZipCode(checkoutEntity.ZipCode);
        }
    }
}
